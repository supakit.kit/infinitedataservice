package com.thailife.InfiniteDataService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfiniteDataServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfiniteDataServiceApplication.class, args);
	}

}
