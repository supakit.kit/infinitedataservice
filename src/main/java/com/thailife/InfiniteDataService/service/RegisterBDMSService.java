package com.thailife.InfiniteDataService.service;

import com.thailife.InfiniteDataService.model.AddressM;
import com.thailife.InfiniteDataService.model.FamilyM;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceRequestT;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceResponse;
import com.thailife.InfiniteDataService.model.requestM.RegisterBDMSRequestM;

import java.util.ArrayList;

public interface RegisterBDMSService {
  DataServiceResponse service(DataServiceRequestT<RegisterBDMSRequestM> requestT);

  boolean validateRequest(DataServiceRequestT<RegisterBDMSRequestM> requestT);

  boolean validateBody(RegisterBDMSRequestM body);

  boolean validateListAddress(ArrayList<AddressM> list);

  boolean validateListFamilies(ArrayList<FamilyM> list);
}
