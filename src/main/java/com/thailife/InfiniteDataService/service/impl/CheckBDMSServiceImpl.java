package com.thailife.InfiniteDataService.service.impl;

import com.thailife.InfiniteDataService.config.DBConnection;
import com.thailife.InfiniteDataService.jdbc.customer.*;
import com.thailife.InfiniteDataService.jdbc.customer.bdms.RegisterJdbc;
import com.thailife.InfiniteDataService.jdbc.policy.PolicyJdbc;
import com.thailife.InfiniteDataService.model.AddressM;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceRequestT;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceResponseT;
import com.thailife.InfiniteDataService.model.jdbcM.LookUpM;
import com.thailife.InfiniteDataService.model.jdbcM.MemberM;
import com.thailife.InfiniteDataService.model.jdbcM.PolicyM;
import com.thailife.InfiniteDataService.model.requestM.CheckBDMSRequestM;
import com.thailife.InfiniteDataService.model.resonseM.CheckBDMSResponseM;
import com.thailife.InfiniteDataService.service.CheckBDMSService;
import com.thailife.InfiniteDataService.utility.Contains;
import com.thailife.InfiniteDataService.utility.RespStatusUtils;
import com.thailife.InfiniteDataService.utility.Utils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

@Service
@Log4j2
public class CheckBDMSServiceImpl implements CheckBDMSService {
  @Autowired private PolicyParticipantJdbc participantJdbc;
  @Autowired private PolicyJdbc policyJdbc;
  @Autowired private InfiniteJdbc infiniteJdbc;
  @Autowired private RegisterJdbc registerJdbc;
  @Autowired private PartyJdbc partyJdbc;
  @Autowired private LookUpJdbc lookUpJdbc;
  @Autowired private CustomerAccountJdbc accountJdbc;

  @Override
  public DataServiceResponseT<CheckBDMSResponseM> service(
      DataServiceRequestT<CheckBDMSRequestM> requestT) {
    DataServiceResponseT<CheckBDMSResponseM> response = new DataServiceResponseT<>();
    log.info(" [START] serviceImplement checkBDMS ==> ");
    try {
      if (validateRequest(requestT)) {
        response.setResponseStatus(
            RespStatusUtils.error(
                Contains.ErrorCode.BDMS01_0005, Contains.ErrorMessage.inputInvalid));
      } else {
        response.setHeaderData(Utils.setResponseTime(requestT.getHeaderData()));
        log.info("request: " + Utils.convertObjectToJsonPretty(requestT));
        String customerId = requestT.getRequestRecord().getCustomerId();
        ArrayList<PolicyM> policyMS = participantJdbc.searchPolicyNoByPartyId(customerId);
        int statusRegister = 0;
        if (policyMS != null && policyMS.size() > 0) {
          log.debug(" List policy of party " + customerId + " | [ " + policyMS + "]");
          /* loop check VP1-VP2 and VIP DD */
          //          for (PolicyM tmp : policyMS) {
          //            if (statusRegister == 0) {
          //              if (Utils.isHealthFitUltra(tmp.getPolicyNo())) {
          //                // VP1-VP2
          //                statusRegister = 4;
          //              } else {
          //                if (Utils.isVipOrDD(tmp.getPolicyNo())) {
          //                  /* vip => { M.stou("วพ1"), M.stou("วพ2"), M.stou("วพ3"),
          // M.stou("วพ4"), M.stou("วพ6"),
          //                  M.stou("วพ7"), M.stou("วพ8"), M.stou("ว01"), M.stou("ว02"),
          // M.stou("ว03")}*/
          //                  /* dd => {"DD3", "DD4"} */
          //                  statusRegister = 3;
          //                }
          //              }
          //            } else {
          //              break;
          //            }
          //          }
          /* loop check VP1-VP2 and VIP DD */

          //          if (statusRegister == 0) {
          /* query check SPH-SPV and Infinite DB */
          //            if (policyJdbc.hasRiderTypeSPHOrSPVByPolicyNo(policyMS)) {
          //              /* search rider join indrider where (SPH, SPV) */
          //              statusRegister = 2;
          //            } else {
          if (infiniteJdbc.hasPartyId(customerId)) {
            /* search infiniteDB where programId = 2 */
            statusRegister = 1;
          }
          //            }
          /* query check SPH-SPV and Infinite DB */
          //          }

          // if statusRegister empty it's mean not found riderType
          // but not go to else.
          log.info(
              " <== statusRegister: " + (statusRegister == 0 ? "empty" : statusRegister) + " ==>");
          if (statusRegister == 0) {
            log.debug(" <== not found. ==> ");
            response.setResponseStatus(
                RespStatusUtils.success(
                    Contains.ErrorCode.BDMS01_0003, Contains.ErrorMessage.permissionDenie));
          } else {
            log.debug(" start query Database.");
            try (Connection con = DBConnection.getConnection(Contains.DbName.customerDB)) {
              CheckBDMSResponseM responseM = partyJdbc.searchPersonByPartyId(customerId, con);
              /* search from register BDMS */
              MemberM memberM = registerJdbc.searchRegisterByPartyId(customerId, con);
              if (memberM != null) {
                responseM.setMemberType(memberM.getMemberType());
                responseM.setMemberRegister(Utils.replaceDate(memberM.getMemberRegister()));
                responseM.setMemberExp(Utils.replaceDate(memberM.getMemberExp()));
                responseM.setMemberCode(Utils.stringDefault(memberM.getMemberCode()));
                if (Utils.stringIsEmpty(memberM.getApprove())
                    || ("2").equals(memberM.getApprove())) {
                  // check BDMS01-0006
                  boolean is0006 = false;
                  for (PolicyM tmpPol : policyMS) {
                    if (is0006) {
                      break;
                    }
                    // search status of policyNo and rider
                    String polStatus =
                        policyJdbc.searchStatus1ByPolicyNo(
                            tmpPol.getPolicyNo(), tmpPol.getCertNo());
                    String riderStatus =
                        policyJdbc.searchRiderstatusByPolicyNo(tmpPol.getPolicyNo());
                    if (!Utils.stringIsEmpty(polStatus) && !Utils.isNotPolicyStatus(polStatus)) {
                      is0006 = true;
                    }
                    if (statusRegister > 1) {
                      // if policyStatus false continue below condition
                      if (!is0006) {
                        if (!Utils.stringIsEmpty(riderStatus)
                            && !Utils.isNotRiderStatus(riderStatus)) {
                          is0006 = true;
                        }
                      }
                      // if policyStatus and riderStatus is null or empty.
                      if (Utils.stringIsEmpty(polStatus) && Utils.stringIsEmpty(riderStatus)) {
                        is0006 = true;
                      }
                    }
                  }
                  if (is0006) {
                    response.setResponseStatus(
                        RespStatusUtils.error(
                            Contains.ErrorCode.BDMS01_0006,
                            Contains.ErrorMessage.policyNoOrRiderDoNotQualify));
                  } else {
                    response.setResponseStatus(
                        RespStatusUtils.success(
                            Contains.ErrorCode.BDMS01_0001, Contains.ErrorMessage.SUCCESS));
                  }
                } else {
                  log.debug(" search from register_BDMS DB approve not equals empty or 2 ");
                  response.setResponseStatus(
                      RespStatusUtils.success(
                          Contains.ErrorCode.BDMS01_0002, Contains.ErrorMessage.notMember));
                }
              } else {
                log.debug(" search from register_BDMS DB not found");
                response.setResponseStatus(
                    RespStatusUtils.success(
                        Contains.ErrorCode.BDMS01_0002, Contains.ErrorMessage.notMember));
              }
              responseM.setStatusRegister(statusRegister);

              HashMap<String, String> cusAccount =
                  accountJdbc.findMobileAndEmailByPartyId(Integer.parseInt(customerId), con);
              String email = "";
              String phone = "";
              if (cusAccount != null) {
                if (!cusAccount.isEmpty()) {
                  email = cusAccount.get("email");
                  phone = cusAccount.get("mobileNo");
                }
              }

              ArrayList<AddressM> addressMS = partyJdbc.searchAddressByPartyId(customerId, con);
              if (addressMS != null && addressMS.size() != 0) {
                setSubDistrict(addressMS, con);
              }

              responseM.setEmail(email);
              responseM.setMobileNo(phone);
              responseM.setAddress(addressMS);
              response.setResponseRecord(responseM);
            } catch (Exception e) {
              throw new Exception(" ");
            } finally {
              log.debug(" end query Database.");
            }
          }
        } else {
          log.debug(" List policy of party " + customerId + " | size 0 or null ");
          response.setResponseStatus(
              RespStatusUtils.success(
                  Contains.ErrorCode.BDMS01_0003, Contains.ErrorMessage.permissionDenie));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(" [serviceImplement]|errorMessage: " + e.getMessage());
      response.setResponseStatus(
          RespStatusUtils.error(Contains.ErrorCode.BDMS01_0004, Contains.ErrorMessage.intervalTh));
    } finally {
      log.info(" [END] serviceImplement checkBDMS ==> ");
    }
    return response;
  }

  @Override
  public boolean validateRequest(DataServiceRequestT<CheckBDMSRequestM> requestT) {
    if (requestT == null) {
      log.debug(" <== request (is null) invalid ==> ");
      return true;
    } else {
      if (Utils.validateHeaderData(requestT.getHeaderData())) {
        log.debug(" <== headerData invalid ==>");
        return true;
      }
      if (validateBody(requestT.getRequestRecord())) {
        log.debug(" <== requestRecord invalid ==>");
        return true;
      }
    }
    return false;
  }

  void setSubDistrict(ArrayList<AddressM> addressMS, Connection con) throws Exception {
    log.debug(" [Start] setSubDistrict ");
    try {
      for (AddressM tmp : addressMS) {
        LookUpM lookUpM = lookUpJdbc.searchSubDistrictBySubDistrictCode(tmp.getSubDistrict(), con);
        if (lookUpM != null) {
          tmp.setSubDistrict(lookUpM.getSubDistrict());
          tmp.setDistrict(lookUpM.getDistrict());
          tmp.setProvince(lookUpM.getProvince());
          tmp.setZipCode(lookUpM.getZipCode());
        } else {
          tmp.setSubDistrict("");
          tmp.setZipCode("");
        }
      }
    } catch (Exception e) {
      log.error("errorMessage: " + e.getMessage());
      throw e;
    } finally {
      log.debug(" [End] setSubDistrict ");
    }
  }

  @Override
  public boolean validateBody(CheckBDMSRequestM body) {
    if (body == null) {
      log.debug("<== requestRecord (is null) invalid ==> ");
      return true;
    } else {
      if (Utils.stringIsEmpty(body.getCustomerId())) {
        log.debug(" <== customerId (null or empty) invalid ==> ");
        return true;
      } else {
        if (body.getCustomerId().length() > 19) {
          log.debug(" <== customerId (length > 19) invalid ==> ");
          return true;
        }
        if (Utils.isNotNumeric(body.getCustomerId())) {
          log.debug(" <== customerId (not numeric) invalid ==> ");
          return true;
        }
      }
    }
    return false;
  }
}
