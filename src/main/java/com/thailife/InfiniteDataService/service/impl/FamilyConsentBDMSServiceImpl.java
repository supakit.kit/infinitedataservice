package com.thailife.InfiniteDataService.service.impl;

import com.thailife.InfiniteDataService.config.DBConnection;
import com.thailife.InfiniteDataService.jdbc.customer.bdms.RegisterFamilyJdbc;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceRequestT;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceResponse;
import com.thailife.InfiniteDataService.model.requestM.FamilyConsentBDMSReqM;
import com.thailife.InfiniteDataService.service.FamilyConsentBDMSService;
import com.thailife.InfiniteDataService.utility.Contains;
import com.thailife.InfiniteDataService.utility.RespStatusUtils;
import com.thailife.InfiniteDataService.utility.Utils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;

@Log4j2
@Service
public class FamilyConsentBDMSServiceImpl implements FamilyConsentBDMSService {
  @Autowired private RegisterFamilyJdbc familyJdbc;

  @Override
  public DataServiceResponse service(DataServiceRequestT<FamilyConsentBDMSReqM> requestT) {
    log.info(" [START] serviceImplement familyConsentBDMS ==>");
    DataServiceResponse response = new DataServiceResponse();
    try {
      if (validateRequest(requestT)) {
        response.setResponseStatus(
            RespStatusUtils.error(
                Contains.ErrorCode.BDMS03_0002, Contains.ErrorMessage.inputInvalid));
      } else {
        response.setHeaderData(Utils.setResponseTime(requestT.getHeaderData()));
        Integer familyId = Integer.parseInt(requestT.getRequestRecord().getFamilyId());
        try (Connection con = DBConnection.getConnection(Contains.DbName.customerDB)) {
          String consented = familyJdbc.findConsentedByFamilyId(familyId, con);
          if (consented != null) {
            if (("").equals(consented)) {
              familyJdbc.updateConsentedByFamilyId(familyId, con);
            }
            response.setResponseStatus(
                RespStatusUtils.success(
                    Contains.ErrorCode.BDMS03_0001, Contains.ErrorMessage.SUCCESS));
          } else {
            throw new Exception("familyId not found in mkt_campaign.regist_bdms_family DB. ");
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(" [serviceImplement]|errorMessage: " + e.getMessage());
      response.setResponseStatus(
          RespStatusUtils.error(Contains.ErrorCode.BDMS03_0003, Contains.ErrorMessage.interval));
    } finally {
      log.info(" [END] serviceImplement familyConsentBDMS ==>");
    }
    return response;
  }

  @Override
  public boolean validateRequest(DataServiceRequestT<FamilyConsentBDMSReqM> requestT) {
    if (requestT == null) {
      log.debug("<== invalid request is null ==>");
      return true;
    } else {
      if (Utils.validateHeaderData(requestT.getHeaderData())) {
        log.debug("<== invalid header is null ==>");
        return true;
      }
      return validateBody(requestT.getRequestRecord());
    }
  }

  @Override
  public boolean validateBody(FamilyConsentBDMSReqM body) {
    if (body == null) {
      log.debug("<== invalid requestRecord is null ==>");
      return true;
    } else {
      if (Utils.stringIsEmpty(body.getFamilyId())) {
        log.debug("<== invalid familyId is null or empty ==>");
        return true;
      } else {
        if (Utils.isNotNumeric(body.getFamilyId())) {
          log.debug("<== invalid familyId is not numeric ==>");
          return true;
        }
      }
    }
    return false;
  }
}
