package com.thailife.InfiniteDataService.service.impl;

import com.thailife.InfiniteDataService.config.DBConnection;
import com.thailife.InfiniteDataService.jdbc.customer.bdms.RegisterAddressJdbc;
import com.thailife.InfiniteDataService.jdbc.customer.bdms.RegisterFamilyJdbc;
import com.thailife.InfiniteDataService.jdbc.customer.bdms.RegisterJdbc;
import com.thailife.InfiniteDataService.model.AddressM;
import com.thailife.InfiniteDataService.model.FamilyM;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceRequestT;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceResponse;
import com.thailife.InfiniteDataService.model.requestM.RegisterBDMSRequestM;
import com.thailife.InfiniteDataService.service.RegisterBDMSService;
import com.thailife.InfiniteDataService.utility.Contains;
import com.thailife.InfiniteDataService.utility.RespStatusUtils;
import com.thailife.InfiniteDataService.utility.Utils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.util.ArrayList;

@Service
@Log4j2
public class RegisterBDMSServiceImpl implements RegisterBDMSService {
  @Autowired private RegisterJdbc registerJdbc;
  @Autowired private RegisterAddressJdbc addressJdbc;
  @Autowired private RegisterFamilyJdbc familyJdbc;

  @Override
  public DataServiceResponse service(DataServiceRequestT<RegisterBDMSRequestM> requestT) {
    log.info(" [START] serviceImplement registerBDMS ==> ");
    DataServiceResponse response = new DataServiceResponse();
    int registerId = 0;
    int addressIdOfRegister = 0;
    int addressIdOfFamily = 0;
    int familyId = 0;
    try {
      if (validateRequest(requestT)) {
        response.setResponseStatus(
            RespStatusUtils.error(
                Contains.ErrorCode.BDMS02_0002, Contains.ErrorMessage.inputInvalid));
      } else {
        response.setHeaderData(Utils.setResponseTime(requestT.getHeaderData()));
        log.info(" request: " + Utils.convertObjectToJsonPretty(requestT));
        RegisterBDMSRequestM requestM = requestT.getRequestRecord();
        try (Connection con = DBConnection.getConnection(Contains.DbName.customerDB)) {
          registerId = registerJdbc.insertRegisterBDMS(requestT.getRequestRecord(), con);
          log.info(" insert regist_bdmsDB Id: " + registerId);
          if (registerId > 0) {
            if (requestM.getAddress().size() > 0) {
              for (AddressM tmpAddress : requestM.getAddress()) {
                addressIdOfRegister = addressJdbc.insertAddress(tmpAddress, con);
                log.info(
                    " insert regist_bdms_addressDB addressId(register): " + addressIdOfRegister);
                if (addressIdOfRegister > 0) {
                  registerJdbc.updateAddressIdById(addressIdOfRegister, registerId, con);
                } else {
                  throw new Exception(" insertAddress[address] fail.");
                }
              }
            } else {
              throw new Exception(" address size = 0 can't insert to register_bdms_address DB.");
            }

            for (FamilyM tmpFamily : requestM.getFamily()) {
              addressIdOfFamily = addressJdbc.insertAddress(tmpFamily, con);
              log.info(" insert regist_bdms_addressDB addressId(family): " + addressIdOfFamily);
              if (addressIdOfFamily > 0) {
                familyId = familyJdbc.insertFamily(tmpFamily, registerId, addressIdOfFamily, con);
                if (familyId == 0) {
                  throw new Exception(" insertFamily[family] fail. ");
                }
              } else {
                throw new Exception(" insertAddress[family] fail.");
              }
            }
            response.setResponseStatus(
                RespStatusUtils.success(
                    Contains.ErrorCode.BDMS02_0001, Contains.ErrorMessage.success));
          } else {
            throw new Exception(" insert register_bdms fail ");
          }
        }
      }
    } catch (Exception e) {
      rollbackDb(addressIdOfRegister, registerId, addressIdOfFamily, familyId);
      log.error(" [serviceImplement]|errorMessage: " + e.getMessage());
      response.setResponseStatus(
          RespStatusUtils.error(Contains.ErrorCode.BDMS02_0003, Contains.ErrorMessage.interval));
    } finally {
      log.info(" [END] serviceImplement registerBDMS ==> ");
    }
    return response;
  }

  private void rollbackDb(
      Integer addressIdOfRegister,
      Integer registerId,
      Integer addressIdOfFamily,
      Integer familyId) {
    try (Connection con = DBConnection.getConnection(Contains.DbName.customerDB)) {
      if (registerId > 0) {
        if (addressIdOfRegister == 0) {
          // rollback. regist_bdms by id.
          registerJdbc.deleteById(registerId, con);
        } else {
          if (addressIdOfFamily == 0) {
            // rollback. regist_bdms by id and rollback. regist_bdms_addr by addressIdOfRegister
            registerJdbc.deleteById(registerId, con);
            addressJdbc.deleteById(addressIdOfRegister, con);
          } else {
            if (familyId == 0) {
              addressJdbc.deleteById(addressIdOfFamily, con);
              registerJdbc.deleteById(registerId, con);
              addressJdbc.deleteById(addressIdOfRegister, con);
            }
          }
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  @Override
  public boolean validateRequest(DataServiceRequestT<RegisterBDMSRequestM> requestT) {
    if (requestT == null) {
      log.debug("<== invalid request is null ==>");
      return true;
    } else {
      if (Utils.validateHeaderData(requestT.getHeaderData())) {
        log.debug("<== headerData invalid ==>");
        return true;
      }
      if (requestT.getRequestRecord() == null) {
        log.debug("<== requestRecord (null) invalid ==>");
        return true;
      }
      if (validateBody(requestT.getRequestRecord())) {
        log.debug("<== requestRecord invalid ==>");
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean validateBody(RegisterBDMSRequestM body) {
    // customerID
    if (Utils.stringIsEmpty(body.getCustomerId())) {
      log.debug("<== customerId (null || empty) invalid ==>");
      return true;
    } else {
      if (body.getCustomerId().length() > 19) {
        log.debug("<== customerId (length > 19) invalid ==>");
        return true;
      }
      if (Utils.isNotNumeric(body.getCustomerId())) {
        log.debug("<== customerId (not numeric) invalid ==>");
        return true;
      }
    }
    // govtId
    if (Utils.strIsEmptyOrIsNullOrOversize(body.getGovtId(), 20)) {
      log.debug("<== govtId (null or empty|| length > 20) invalid ==>");
      return true;
    }
    // nationalityCode.
    if (Utils.strIsEmptyOrIsNullOrOversize(body.getNationality(), 100)) {
      log.debug("<== nationalityCode (null or empty || length > 3) invalid ==> ");
      return true;
    }
    // mobileNo.
    if (!Utils.isMobileNo(body.getMobileNo())) {
      log.debug("<== mobileNo (null or empty || length > 10) invalid ==> ");
      return true;
    }
    // email.
    if (Utils.isNullOrNotFormatEmail(body.getEmail())) {
      log.debug("<== email (not format) invalid ==>");
      return true;
    } else {
      if (body.getEmail().length() > 100) {
        log.debug("<== email (length > 100) invalid ==>");
        return true;
      }
    }
    // memberType
    if (body.getMemberType() == null) {
      log.debug("<== memberType (null) invalid ==>");
      return true;
    } else if (1 > body.getMemberType() || body.getMemberType() > 4) {
      log.debug("<== memberType (not 1,2,3,4) invalid ==>");
      return true;
    }
    // registerDate.
    if (Utils.strIsEmptyOrIsNullOrOversize(body.getRegisterDate(), 8)) {
      log.debug("<== registerDate (null or empty || length > 8) invalid ==>");
      return true;
    } else if (Utils.isYYYYMMDD(body.getRegisterDate())) {
      log.debug("<== registerDate invalid ==>");
      return true;
    }
    // list Address.
    if (body.getAddress() == null) {
      log.debug("<== AddressList (null) invalid ==>");
      return true;
    } else if (body.getAddress().size() > 0) {
      if (validateListAddress(body.getAddress())) {
        log.debug("<== addressList invalid ==>");
        return true;
      }
    }
    // list Family
    if (body.getFamily() == null) {
      log.debug("<== familyList (null) invalid ==>");
      return true;
    } else if (body.getFamily().size() > 0) {
      if (validateListFamilies(body.getFamily())) {
        log.debug("<== familyList invalid ==>");
        return true;
      }
    }
    /*DataServiceRequestT<CheckBDMSRequestM> requestT = new DataServiceRequestT<>();

    requestT.setRequestRecord(new CheckBDMSRequestM(body.getCustomerId()));
    requestT.setHeaderData(Utils.getHeader());
    DataServiceResponseT<CheckBDMSResponseM> responseT = checkBDMSService.service(requestT);
    if (responseT.getResponseStatus() != null) {
      if (!("BDMS01-0002").equals(responseT.getResponseStatus().getErrorCode())) {
        log.debug("<== customerId (not approve) invalid ==>");
        return true;
      } else {
        if (responseT.getResponseRecord() == null) {
          log.debug("<== check partyId (responseRecord is null) invalid ==> ");
          return true;
        } else {
          CheckBDMSResponseM responseM = responseT.getResponseRecord();
          if (!(body.getMemberType()).equals(responseM.getStatusRegister())) {
            log.debug("<== partyId with memberType (not match) invalid ==> ");
            return true;
          }
          if (!(body.getGovtId()).equals(responseM.getGovtId())) {
            log.debug("<== partyId with govtId (not match) invalid ==> ");
            return true;
          }
        }
      }
    }*/
    return false;
  }

  @Override
  public boolean validateListAddress(ArrayList<AddressM> list) {
    int i = 0;
    for (AddressM tmp : list) {
      log.debug("<== addressList invalid [position: " + i + "] ==>");
      if (validateAddressM(tmp)) {
        return true;
      }
      i++;
    }
    return false;
  }

  @Override
  public boolean validateListFamilies(ArrayList<FamilyM> list) {
    int i = 0;
    for (FamilyM tmp : list) {
      log.debug("<== familyList invalid [position: " + i + "] ==>");
      if (Utils.strIsNullOrIsOversize(tmp.getPreName(), 20)) {
        log.debug("<== preName (null || length > 20) invalid ==>");
        return true;
      }

      if (Utils.strIsNullOrIsOversize(tmp.getFirstName(), 50)) {
        log.debug("<== firstName (null || length > 50) invalid ==>");
        return true;
      }

      if (Utils.strIsNullOrIsOversize(tmp.getLastName(), 50)) {
        log.debug("<== lastName (null || length > 50) invalid ==>");
        return true;
      }

      if (Utils.strIsNullOrIsOversize(tmp.getBirthDate(), 8)) {
        log.debug("<== birthDate (null || length > 8) invalid ==>");
        return true;
      } else if (Utils.isYYYYMMDD(tmp.getBirthDate())) {
        log.debug("<== birthDate (not format) invalid: " + tmp.getBirthDate());
        return true;
      }

      if (Utils.strIsNullOrIsOversize(tmp.getSex(), 10)) {
        log.debug("<== sex (null || length > 10) invalid ==>");
        return true;
      }

      if (!Utils.isMobileNo(tmp.getMobileNo())) {
        log.debug("<== mobileNo (null || length > 10) invalid ==>");
        return true;
      }

      if (tmp.getRelationCode() == null) {
        log.debug("<== relationCode (null) invalid ==>");
        return true;
      } else if (0 > tmp.getRelationCode() || tmp.getRelationCode() > 99) {
        log.debug("<== relationCode (0 > || relationCode || < 99) invalid ==> ");
        return true;
      }

      if (Utils.strIsNullOrIsOversize(tmp.getRelation(), 20)) {
        log.debug("<== relation (null || length > 20) invalid ==>");
        return true;
      }

      if (validateAddressM(tmp)) {
        return true;
      }

      i++;
    }
    return false;
  }

  private boolean validateAddressM(AddressM tmp) {
    if (Utils.strIsNullOrIsOversize(tmp.getPlace(), 200)) {
      log.debug("<== place (null || length > 200) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getHouseNo(), 100)) {
      log.debug("<== houseNo (null || length > 100) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getBuilding(), 200)) {
      log.debug("<== building (null || length > 200) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getFloor(), 100)) {
      log.debug("<== floor (null || length > 100) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getRoomNo(), 100)) {
      log.debug("<== roomNo (null || length > 100) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getVillage(), 100)) {
      log.debug("<== village (null || length > 100) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getVillageNo(), 100)) {
      log.debug("<== villageNo (null || length > 100) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getAlley(), 100)) {
      log.debug("<== alley (null || length > 100) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getRoad(), 100)) {
      log.debug("<== road (null || length > 100) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getDistrict(), 100)) {
      log.debug("<== district (null || length > 100) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getSubDistrict(), 100)) {
      log.debug("<== subDistrict (null || length > 100) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getZipCode(), 5)) {
      log.debug("<== zipCode (null || length > 100) invalid ==>");
      return true;
    }
    if (Utils.strIsNullOrIsOversize(tmp.getProvince(), 100)) {
      log.debug("<== province (null || length > 100) invalid ==>");
      return true;
    }

    return false;
  }
}
