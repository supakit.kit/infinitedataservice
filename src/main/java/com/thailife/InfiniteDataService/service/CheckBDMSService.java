package com.thailife.InfiniteDataService.service;

import com.thailife.InfiniteDataService.model.commonModel.DataServiceRequestT;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceResponseT;
import com.thailife.InfiniteDataService.model.requestM.CheckBDMSRequestM;
import com.thailife.InfiniteDataService.model.resonseM.CheckBDMSResponseM;

public interface CheckBDMSService {
  DataServiceResponseT<CheckBDMSResponseM> service(DataServiceRequestT<CheckBDMSRequestM> requestT);

  boolean validateRequest(DataServiceRequestT<CheckBDMSRequestM> requestT);

  boolean validateBody(CheckBDMSRequestM body);
}
