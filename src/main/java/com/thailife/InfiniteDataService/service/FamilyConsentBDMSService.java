package com.thailife.InfiniteDataService.service;

import com.thailife.InfiniteDataService.model.commonModel.DataServiceRequestT;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceResponse;
import com.thailife.InfiniteDataService.model.requestM.FamilyConsentBDMSReqM;

public interface FamilyConsentBDMSService {
  DataServiceResponse service(DataServiceRequestT<FamilyConsentBDMSReqM> requestT);

  boolean validateRequest(DataServiceRequestT<FamilyConsentBDMSReqM> requestT);

  boolean validateBody(FamilyConsentBDMSReqM body);
}
