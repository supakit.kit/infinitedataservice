package com.thailife.InfiniteDataService.jdbc.customer;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

@Component
@Log4j2
public class CustomerAccountJdbc {
  public HashMap<String, String> findMobileAndEmailByPartyId(Integer partyId, Connection con)
      throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" select mobile_no, email_address ");
    sql.append(" from customer_account.account ");
    sql.append(" where party_id = ? order by create_time desc ");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, partyId);
      try (ResultSet rs = pst.executeQuery()) {
        HashMap<String, String> map = new HashMap<>();
        if (rs.next()) {
          map.put("mobileNo", rs.getString("mobile_no"));
          map.put("email", rs.getString("email_address"));
          return map;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
    return null;
  }
}
