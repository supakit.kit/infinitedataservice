package com.thailife.InfiniteDataService.jdbc.customer.bdms;

import com.thailife.InfiniteDataService.model.jdbcM.MemberM;
import com.thailife.InfiniteDataService.model.requestM.RegisterBDMSRequestM;
import com.thailife.InfiniteDataService.utility.Utils;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
@Log4j2
public class RegisterJdbc {
  public MemberM searchRegisterByPartyId(String partyId, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" select member_type, member_code, expired_date, register_date, approve ");
    sql.append(" from mkt_campaign.regist_bdms ");
    sql.append(" where party_id = ? order by create_date desc ");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, Integer.parseInt(partyId));
      try (ResultSet rs = pst.executeQuery()) {
        if (rs.next()) {
          return new MemberM(
              rs.getString("register_date"),
              rs.getString("expired_date"),
              rs.getString("member_code"),
              rs.getInt("member_type"),
              rs.getString("approve"));
        } else {
          return null;
        }
      }
    } catch (Exception e) {
      throw new Exception(" searchRegistBdmsByPartyId fail: " + e.getMessage());
    }
  }

  public Integer insertRegisterBDMS(RegisterBDMSRequestM requestM, Connection con)
      throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" insert into mkt_campaign.regist_bdms ");
    sql.append(" ( party_id, nationality, govtid, member_type, ");
    sql.append(" register_date, create_date, update_date, mobile_no, email ) ");
    sql.append(" values (?, ?, ?, ?, ?, now(), now(), ?, ?)");
    log.debug("sql: " + sql);

    try (PreparedStatement pst =
        con.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
      pst.setInt(1, Integer.parseInt(requestM.getCustomerId()));
      pst.setString(2, requestM.getNationality());
      pst.setString(3, requestM.getGovtId());
      pst.setInt(4, requestM.getMemberType());
      pst.setDate(5, Date.valueOf(Utils.yyyyMMddToYyyy_MM_dd(requestM.getRegisterDate())));
      pst.setString(6, requestM.getMobileNo());
      pst.setString(7, requestM.getEmail());
      pst.executeUpdate();
      try (ResultSet rs = pst.getGeneratedKeys()) {
        if (rs.next()) {
          return rs.getInt("id");
        } else {
          throw new Exception(" insertByRegisterBDMSRequest fail. ");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  public void updateAddressIdById(Integer addressId, Integer id, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" update mkt_campaign.regist_bdms ");
    sql.append(" set address_id = ?, update_date=now() ");
    sql.append(" where id = ? ");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, addressId);
      pst.setInt(2, id);
      if (pst.executeUpdate() == 0) {
        throw new Exception(" update regist_bdms fail.");
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception(" update registerBDMS | " + e.getMessage());
    }
  }

  public void deleteById(Integer id, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" delete from mkt_campaign.regist_bdms");
    sql.append(" where id = ? ");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, id);
      pst.executeUpdate();
    } catch (Exception e) {
      throw new Exception(" deleteById | " + e.getMessage());
    }
  }
}
