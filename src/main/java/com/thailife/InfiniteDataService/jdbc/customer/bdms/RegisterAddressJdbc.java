package com.thailife.InfiniteDataService.jdbc.customer.bdms;

import com.thailife.InfiniteDataService.model.AddressM;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

@Log4j2
@Component
public class RegisterAddressJdbc {

  public Integer insertAddress(AddressM addressM, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" insert into mkt_campaign.regist_bdms_addr ");
    sql.append(" ( place, house_no, moo, building, room_no, ");
    sql.append(" floor, village, alley, road, sub_district, ");
    sql.append(" district, province, zip_code, create_date ) ");
    sql.append(" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now())");
    log.debug("sql: " + sql);
    try (PreparedStatement pst =
        con.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
      int i = 1;
      pst.setString(i++, addressM.getPlace());
      pst.setString(i++, addressM.getHouseNo());
      pst.setString(i++, addressM.getVillageNo());
      pst.setString(i++, addressM.getBuilding());
      pst.setString(i++, addressM.getRoomNo());
      pst.setString(i++, addressM.getFloor());
      pst.setString(i++, addressM.getVillage());
      pst.setString(i++, addressM.getAlley());
      pst.setString(i++, addressM.getRoad());
      pst.setString(i++, addressM.getSubDistrict());
      pst.setString(i++, addressM.getDistrict());
      pst.setString(i++, addressM.getProvince());
      pst.setString(i, addressM.getZipCode());
      pst.executeUpdate();
      try (ResultSet rs = pst.getGeneratedKeys()) {
        if (rs.next()) {
          return rs.getInt("address_id");
        } else {
          throw new Exception(" insertByAddressM fail. ");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  public void deleteById(Integer id, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" delete from mkt_campaign.regist_bdms_addr ");
    sql.append(" where address_id = ? ");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, id);
      pst.executeUpdate();
    } catch (Exception e) {
      throw new Exception(" deleteById |  " + e.getMessage());
    }
  }
}
