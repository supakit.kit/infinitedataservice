package com.thailife.InfiniteDataService.jdbc.customer;

import com.thailife.InfiniteDataService.model.jdbcM.LookUpM;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Component
@Log4j2
public class LookUpJdbc {
  public LookUpM searchSubDistrictBySubDistrictCode(String subDistrictCode, Connection con)
      throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" select sub_district as subDistrict, zip_code as zipCode, ");
    sql.append(" district, province, region ");
    sql.append(" from lookup.sub_district ls ");
    sql.append(" where ls.sub_district_code = ? limit 1");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, Integer.parseInt(subDistrictCode));
      try (ResultSet rs = pst.executeQuery()) {
        if (rs.next()) {
          return new LookUpM(
              rs.getString("subDistrict"),
              rs.getString("district"),
              rs.getString("zipCode"),
              rs.getString("province"),
              rs.getString("region"));
        } else {
          return null;
        }
      }
    } catch (Exception e) {
      log.error("errorMessage: " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
  }
}
