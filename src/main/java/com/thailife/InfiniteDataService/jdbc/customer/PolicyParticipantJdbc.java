package com.thailife.InfiniteDataService.jdbc.customer;

import com.thailife.InfiniteDataService.config.DBConnection;
import com.thailife.InfiniteDataService.model.jdbcM.PolicyM;
import com.thailife.InfiniteDataService.utility.Contains;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@Component
@Log4j2
public class PolicyParticipantJdbc {
  public ArrayList<PolicyM> searchPolicyNoByPartyId(String partyId) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append("  select pp.policy_no as policyNo, pp.cert_no as certNo");
    sql.append("  from policy.policy_participant pp ");
    sql.append("  where party_id = ? ");
    log.debug("sql: " + sql);
    try (Connection con = DBConnection.getConnection(Contains.DbName.customerDB)) {
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setInt(1, Integer.parseInt(partyId));
        try (ResultSet rs = pst.executeQuery()) {
          ArrayList<PolicyM> policyMS = new ArrayList<>();
          while (rs.next()) {
            policyMS.add(new PolicyM(rs.getString("policyNo"), rs.getString("certNo")));
          }
          return policyMS;
        }
      }
    } catch (Exception e) {
      log.error("errorMessage: " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
  }
}
