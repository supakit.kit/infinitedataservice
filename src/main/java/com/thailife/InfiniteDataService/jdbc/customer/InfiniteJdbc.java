package com.thailife.InfiniteDataService.jdbc.customer;

import com.thailife.InfiniteDataService.config.DBConnection;
import com.thailife.InfiniteDataService.utility.Contains;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Component
@Log4j2
public class InfiniteJdbc {
  public boolean hasPartyId(String partyId) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" select 1 ");
    sql.append(" from infinite.cust_program_stat cps ");
    sql.append(" where cps.program_id = 2 and cps.party_id = ? ");
    try (Connection con = DBConnection.getConnection(Contains.DbName.customerDB)) {
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setInt(1, Integer.parseInt(partyId));
        try (ResultSet rs = pst.executeQuery()) {
          return rs.next();
        }
      }
    } catch (Exception e) {
      log.error(" errorMessage: " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
  }
}
