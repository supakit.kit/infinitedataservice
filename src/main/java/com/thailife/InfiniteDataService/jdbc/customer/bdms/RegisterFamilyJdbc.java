package com.thailife.InfiniteDataService.jdbc.customer.bdms;

import com.thailife.InfiniteDataService.model.FamilyM;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

@Log4j2
@Component
public class RegisterFamilyJdbc {

  public String findConsentedByFamilyId(Integer familyId, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" select consented from mkt_campaign.regist_bdms_family where family_id = ? ");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, familyId);
      try (ResultSet rs = pst.executeQuery()) {
        if (rs.next()) {
          return rs.getString("consented");
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
    return null;
  }

  public Integer insertFamily(FamilyM family, Integer id, Integer addrId, Connection con)
      throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" insert into mkt_campaign.regist_bdms_family");
    sql.append(" ( id, pre_name, first_name, last_name,");
    sql.append(" birth_date, sex, mobileNo,");
    sql.append(" relation, address_id, create_date, update_date)");
    sql.append(" values (?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now())");
    log.debug("sql: " + sql);
    try (PreparedStatement pst =
        con.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
      int i = 1;
      pst.setInt(i++, id);
      pst.setString(i++, family.getPreName());
      pst.setString(i++, family.getFirstName());
      pst.setString(i++, family.getLastName());
      pst.setString(i++, family.getBirthDate());
      pst.setString(i++, family.getSex());
      pst.setString(i++, family.getMobileNo());
      pst.setString(i++, family.getRelation());
      pst.setInt(i, addrId);
      pst.executeUpdate();
      try (ResultSet rs = pst.getGeneratedKeys()) {
        if (rs.next()) {
          return rs.getInt("id");
        } else {
          throw new Exception(" insertByFamilyM fail. ");
        }
      }
    } catch (Exception e) {
      throw new Exception(" insertFamily | " + e.getMessage());
    }
  }

  public void updateConsentedByFamilyId(Integer familyId, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" update mkt_campaign.regist_bdms_family ");
    sql.append(" set consented = '1' ");
    sql.append(" where family_id = ? ");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, familyId);
      pst.executeUpdate();
    } catch (Exception e) {
      throw new Exception(" updateAddressIdByFamilyId | " + e.getMessage());
    }
  }
}
