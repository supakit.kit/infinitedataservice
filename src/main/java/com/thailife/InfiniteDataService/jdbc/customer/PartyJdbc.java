package com.thailife.InfiniteDataService.jdbc.customer;

import com.thailife.InfiniteDataService.config.DBConnection;
import com.thailife.InfiniteDataService.model.AddressM;
import com.thailife.InfiniteDataService.model.resonseM.CheckBDMSResponseM;
import com.thailife.InfiniteDataService.utility.Contains;
import com.thailife.InfiniteDataService.utility.Utils;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@Component
@Log4j2
public class PartyJdbc {

  public boolean hasPartyId(String partyId) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" select 1 from party.party");
    sql.append(" where party_id = ? ");
    try (Connection con = DBConnection.getConnection(Contains.DbName.customerDB)) {
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setInt(1, Integer.parseInt(partyId));
        try (ResultSet rs = pst.executeQuery()) {
          return rs.next();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }

  public ArrayList<AddressM> searchAddressByPartyId(String partyId, Connection con)
      throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" select a.place, a.house_number as houseNo, ");
    sql.append(" a.building, a.floor, a.room, a.village_name as village, ");
    sql.append(" a.village_number as villageNo, a.housing, a.lane,  ");
    sql.append(" a.alley, a.road, a.sub_district as subDistrict, ");
    sql.append(" a.zip_code as zipCode, a.last_update ");
    sql.append(" from party.party_address pa right join contact.address a  ");
    sql.append(" on pa.address_id = a.address_id ");
    sql.append(" where pa.party_id = ? ");
    sql.append(" order by a.last_update ");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, Integer.parseInt(partyId));
      try (ResultSet rs = pst.executeQuery()) {
        ArrayList<AddressM> addressMS = new ArrayList<>();
        while (rs.next()) {
          addressMS.add(
              new AddressM(
                  rs.getString("place"),
                  rs.getString("houseNo"),
                  rs.getString("building"),
                  rs.getString("floor"),
                  rs.getString("room"),
                  rs.getString("village"),
                  rs.getString("villageNo"),
                  rs.getString("alley"),
                  rs.getString("road"),
                  "",
                  rs.getString("subDistrict"),
                  rs.getString("zipCode"),
                  ""));
        }
        return addressMS;
      }
    } catch (Exception e) {
      log.error("errorMessage: " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
  }

  public String searchEmailByPartyId(String partyId, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" select e.addr_line as email ");
    sql.append(" from party.party_email pe ");
    sql.append(" right join contact.email e on ");
    sql.append(" pe.email_id = e.email_id ");
    sql.append(" where pe.party_id = ? ");
    sql.append(" order by e.last_update desc limit 1 ");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, Integer.parseInt(partyId));
      try (ResultSet rs = pst.executeQuery()) {
        if (rs.next()) {
          return rs.getString("email");
        }
      }
    } catch (Exception e) {
      log.error("errorMessage: " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
    return "";
  }

  public String searchPhoneByPartyId(String partyId, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" select p.phone_number as phoneNo ");
    sql.append(" from party.party_phone pp ");
    sql.append(" right join contact.phone p on ");
    sql.append(" pp.phone_id = p.phone_id ");
    sql.append(" where pp.party_id = ? order by p.last_update desc limit 1 ");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, Integer.parseInt(partyId));
      try (ResultSet rs = pst.executeQuery()) {
        if (rs.next()) {
          return rs.getString("phoneNo");
        }
      }
    } catch (Exception e) {
      log.error("errorMessage: " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
    return "";
  }

  public CheckBDMSResponseM searchPersonByPartyId(String partyId, Connection con) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" select pp.party_id as partyId, pp.govt_id as govtId, p.birth_date as dob, ");
    sql.append(" p.pname_en as preNameEn, p.fname_en as firstNameEn, p.lname_en as lastNameEn,");
    sql.append(" p.pname_th as preNameTh, p.fname_th as firstNameTh, p.lname_th as lastNameTh, ");
    sql.append(" p.citizen as nationalityCode ");
    sql.append(" from party.party pp ");
    sql.append(" left join party.person p on pp.party_id = p.party_id ");
    sql.append(" where pp.party_id = ? order by p.last_update desc ");
    log.debug("sql: " + sql);
    try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
      pst.setInt(1, Integer.parseInt(partyId));
      try (ResultSet rs = pst.executeQuery()) {
        if (rs.next()) {
          return new CheckBDMSResponseM(
              rs.getString("partyId"),
              rs.getString("govtId"),
              rs.getString("preNameTh"),
              rs.getString("firstNameTh"),
              rs.getString("lastNameTh"),
              rs.getString("firstNameEn"),
              rs.getString("lastNameEn"),
              Utils.replaceDate(rs.getString("dob")),
              rs.getString("nationalityCode"));
        }
      }
    } catch (Exception e) {
      log.error("errorMessage: " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
    return null;
  }
}
