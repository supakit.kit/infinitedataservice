package com.thailife.InfiniteDataService.jdbc.policy;

import com.thailife.InfiniteDataService.config.DBConnection;
import com.thailife.InfiniteDataService.model.jdbcM.PolicyM;
import com.thailife.InfiniteDataService.utility.Contains;
import com.thailife.InfiniteDataService.utility.Utils;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@Component
@Log4j2
public class PolicyJdbc {
  public boolean hasRiderTypeSPHOrSPVByPolicyNo(ArrayList<PolicyM> listPolicy) throws Exception {
    StringBuilder sqlCon = new StringBuilder();
    for (PolicyM tmp : listPolicy) {
      Utils.setQueryUpdate(sqlCon, "'" + tmp.getPolicyNo() + "'");
    }
    StringBuilder sql = new StringBuilder();
    sql.append("  select policyno as policyNo, ridertype as riderType , 'rider' as table ");
    sql.append("  from mstpolicy.rider r ");
    sql.append("  where ridertype in ('SPH', 'SPV') ");
    sql.append("  and policyno in (").append(sqlCon).append(")");
    sql.append("  union");
    sql.append("  select policyno as policyNo, ridertype, 'indRider' as table");
    sql.append("  from mstpolicy.indrider i ");
    sql.append("  where  ridertype in ('SPH', 'SPV') ");
    sql.append("  and policyno in (").append(sqlCon).append(")");
    log.debug("sql: " + sql);
    try (Connection con = DBConnection.getConnection(Contains.DbName.policyDB)) {
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        try (ResultSet rs = pst.executeQuery()) {
          return rs.next();
        }
      }
    } catch (Exception e) {
      log.error("errorMessage: " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
  }

  public String searchRiderstatusByPolicyNo(String policyNo) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append("  select riderstatus as riderStatus ");
    sql.append("  from mstpolicy.rider r ");
    sql.append("  where policyno = ? ");
    sql.append("  union ");
    sql.append("  select riderstatus as riderStatus ");
    sql.append("  from mstpolicy.indrider i ");
    sql.append("  where  policyno = ? ");
    sql.append(" limit 1 ");
    log.debug("sql: " + sql);
    try (Connection con = DBConnection.getConnection(Contains.DbName.policyDB)) {
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        pst.setString(1, policyNo);
        pst.setString(2, policyNo);
        try (ResultSet rs = pst.executeQuery()) {
          if (rs.next()) {
            return rs.getString("riderStatus");
          } else {
            return null;
          }
        }
      }
    } catch (Exception e) {
      log.error("errorMessage: " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
  }

  public String searchStatus1ByPolicyNo(String policyNo, String certNo) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append("select policystatus1 as status1 ");
    sql.append("from mstpolicy.indmast where policyno = ? ");
    sql.append("union ");
    sql.append("select policystatus1 as status1 ");
    sql.append("from mstpolicy.ordmast where policyno = ? ");
    sql.append("union ");
    sql.append("select policystatus1 as status1 ");
    sql.append("from mstpolicy.whlmast where policyno = ? ");
    sql.append("union ");
    sql.append("select policystatus1 as status1 ");
    sql.append("from unitlink.unitlink where policyno = ? ");
    sql.append("union ");
    sql.append("select policystatus1 as status1 ");
    sql.append("from universal.universallife where policyno = ? ");
    sql.append("union ");
    sql.append("select statcer as status1 ");
    sql.append("from mortgage.cert where policyno = ? and certno = ? ");
    sql.append("LIMIT 1 ");
    log.debug("sql: " + sql);
    try (Connection con = DBConnection.getConnection(Contains.DbName.policyDB)) {
      try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
        for (int i = 1; i <= 7; i++) {
          if (i < 7) {
            pst.setString(i, policyNo);
          } else {
            pst.setString(i, certNo);
          }
        }
        try (ResultSet rs = pst.executeQuery()) {
          if (rs.next()) {
            return rs.getString("status1");
          } else {
            return null;
          }
        }
      }
    } catch (Exception e) {
      log.error("errorMessage: " + e.getMessage());
      e.printStackTrace();
      throw e;
    }
  }
}
