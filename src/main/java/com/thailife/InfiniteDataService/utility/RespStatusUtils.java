package com.thailife.InfiniteDataService.utility;

import com.thailife.InfiniteDataService.model.commonModel.ResponseStatus;

public class RespStatusUtils {
  public static ResponseStatus success() {
    return new ResponseStatus("S", "200", Contains.ErrorMessage.success);
  }

  public static ResponseStatus success(String msg) {
    return new ResponseStatus("S", "200", msg);
  }

  public static ResponseStatus success(String errorCode, String msg) {
    return new ResponseStatus("S", errorCode, msg);
  }

  public static ResponseStatus success(String statusCode, String errorCode, String msg) {
    return new ResponseStatus(statusCode, errorCode, msg);
  }

  public static ResponseStatus error() {
    return new ResponseStatus("E", "500", Contains.ErrorMessage.interval);
  }

  public static ResponseStatus error(String msg) {
    return new ResponseStatus("E", "500", msg);
  }

  public static ResponseStatus error(String errorCode, String msg) {
    return new ResponseStatus("E", errorCode, msg);
  }

  public static ResponseStatus error(String statusCode, String errorCode, String msg) {
    return new ResponseStatus(statusCode, errorCode, msg);
  }

  public static ResponseStatus errorValidate() {
    return new ResponseStatus("E", "401", Contains.ErrorMessage.badRequest);
  }

  public static ResponseStatus successNotfoundData() {
    return new ResponseStatus("S", "204", Contains.ErrorMessage.dataNotFound);
  }

  public static ResponseStatus errorInternalThServer() {
    return new ResponseStatus("E", "500", Contains.ErrorMessage.intervalTh);
  }

  public static ResponseStatus searchSuccess() {
    return new ResponseStatus("S", "200", Contains.ErrorMessage.searchSuccess);
  }
}
