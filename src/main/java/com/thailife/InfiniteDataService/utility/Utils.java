package com.thailife.InfiniteDataService.utility;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thailife.InfiniteDataService.model.commonModel.HeaderData;
import com.thailife.InfiniteDataService.model.commonModel.ResponseStatus;
import lombok.extern.log4j.Log4j2;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Log4j2
public class Utils {
  public static boolean stringIsEmpty(String str) {
    return str == null || "".equals(str.trim()) || "null".equals(str.trim());
  }

  public static boolean isMobileNo(String str) {
    return str.matches("\\d{10}");
  }

  public static boolean isNotNumeric(String str) {
    try {
      Long.parseLong(str);
      return false;
    } catch (Exception e) {
      return true;
    }
  }

  public static String yyyyMMddToYyyy_MM_dd(String strDate) {
    SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
    try {
      // convert year th to en
      Calendar cal = Calendar.getInstance();
      cal.setTime(yyyyMMdd.parse(strDate));
      cal.add(Calendar.YEAR, -543); // - 543 to year EN.
      return yyyy_MM_dd.format(cal.getTime());
    } catch (Exception e) {
      e.printStackTrace();
      return yyyy_MM_dd.format(new Date());
    }
  }

  public static boolean isNullOrNotFormatEmail(String email) {
    String regex =
        "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    return (email == null) || !email.matches(regex);
  }

  public static boolean isYYYYMMDD(String date) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    try {
      dateFormat.parse(date);
      return false;
    } catch (Exception e) {
      log.debug(" Input format(yyyyMMdd) date invalid value: " + date);
      return true;
    }
  }

  public static boolean strIsNullOrIsOversize(String value, Integer len) {
    return (value == null || value.length() > len);
  }

  public static boolean strIsEmptyOrIsNullOrOversize(String value, Integer leg) {
    return (value == null || ("").equals(value) || value.length() > leg);
  }

  public static int getHttpCode(ResponseStatus responseStatus) {
    if (responseStatus == null) return 200;
    return "500".equals(responseStatus.getErrorCode()) ? 500 : 200;
  }

  public static String now() {
    return getToday(Contains.PattenDate.yyyy_MM_dd_HHMmSs + ".SSS");
  }

  public static String getToday(String patten) {
    SimpleDateFormat wsFormat = new SimpleDateFormat(patten);
    return wsFormat.format(new Date());
  }

  public static void setQueryUpdate(StringBuilder sqlSet, String updateData) {
    if (sqlSet.length() != 0) {
      sqlSet.append(", ");
    }
    sqlSet.append(updateData);
  }

  public static boolean validateHeaderData(HeaderData headerData) {
    if (headerData == null) {
      return true;
    } else if (headerData.getMessageId() == null || ("").equals(headerData.getMessageId())) {
      return true;
    } else if (headerData.getSentDateTime() == null || ("").equals(headerData.getSentDateTime())) {
      return true;
    }
    return (headerData.getMessageId().length() > 50);
  }

  public static HeaderData getHeader() {
    HeaderData header = new HeaderData();
    header.setMessageId(UUID.randomUUID().toString());
    header.setSentDateTime(now());
    return header;
  }

  public static HeaderData setResponseTime(HeaderData headerData) {
    if (headerData == null) {
      headerData = getHeader();
    }
    headerData.setResponseDateTime(now());
    return headerData;
  }

  public static String convertObjectToJsonPretty(Object o) {
    ObjectMapper mapper =
        new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    try {
      if (o == null) return "{ \"Object\" : \"\" }";
      return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(o);
    } catch (Exception e) {
      e.printStackTrace();
      return "{ \"Object\" : \"\" }";
    }
  }

  public static boolean isNotPolicyStatus(String policyStatus) {
    switch (policyStatus) {
      case "C": // บังคับเวนคืน (ยังไม่ตัดเงินกู้จากแฟ้ม)	=> ไม่มีผลบังคับ
      case "D": // มรณกรรม	=> ไม่มีผลบังคับ
      case "G": // ครบกำหนดส่งเข้ากองทุนฯ	=> ไม่มีผลบังคับ
      case "K": // ครบสัญญา สำหรับ Term และ PA	=> ไม่มีผลบังคับ
      case "L": // ขาดผลบังคับ	=> ไม่มีผลบังคับ
      case "M": // รับเงินครบสัญญา	=> ไม่มีผลบังคับ
      case "O": // ขาดผลบังคับจากการขยายเวลา	=> ไม่มีผลบังคับ
      case "P": // เปลี่ยนแบบประกัน	=> ไม่มีผลบังคับ
      case "S": // เวนคืนกรมธรรม์	=> ไม่มีผลบังคับ
      case "T": // ครบกำหนด (ยังไม่รับเงิน)	=> ไม่มีผลบังคับ
      case "X": // บอกล้างสัญญา	=> ไม่มีผลบังคับ
      case "Y": // บังคับเวนคืน (ตัดเงินกู้จากแฟ้มแล้ว)	=> ไม่มีผลบังคับ
      case "Z": // ขาดผลบังคับเกิน 5 ปี	=> ไม่มีผลบังคับ
      case "#": // ยกเลิก Free Look Period	=> ไม่มีผลบังคับ
      case "*": // ยกเลิกอื่นๆ	=> ไม่มีผลบังคับ
        return false;
      default:
        return true;
    }
  }

  public static boolean isNotRiderStatus(String riderStatus) {
    switch (riderStatus) {
      case "A": // สิ้นสุดสัญญา ตามระยะคุ้มครอง   => ไม่มีผลบังคับ
      case "T": // สิ้นสุดสัญญา ตามการร้องขอ  => ไม่มีผลบังคับ
      case "W": // บอกเลิกสัญญา => ไม่มีผลบังคับ
      case "X": // บอกล้างสัญญา  => ไม่มีผลบังคับ
        return false;
      default:
        return true;
    }
  }

  public static String replaceDate(String date) {
    if (date == null) {
      return "";
    } else {
      return date.replace("-", "");
    }
  }

  public static String stringDefault(String str) {
    if (str == null) {
      return "";
    }
    return str;
  }
}
