package com.thailife.InfiniteDataService.utility;

public class Contains {
  public static class PattenDate {
    public static final String yyyy_MM_dd_HHMmSs = "yyyy-MM-dd HH:mm:ss";
  }

  public static class DbName {
    public static final String customerDB = "DBCustomer";
    public static final String policyDB = "DBPolicy";
  }

  public static class ErrorMessage {
    public static final String success = "success";
    public static final String SUCCESS = "SUCCESS";
    public static final String intervalTh = "ระบบขัดข้อง";
    public static final String interval = "error";
    public static final String badRequest = "ระบุข้อมูลไม่ถูกต้อง";
    public static final String dataNotFound = "ไม่พบข้อมูลที่ระบุ";
    public static final String searchSuccess = "ค้นหาสำเร็จ";
    public static final String inputInvalid = "validate error";
    public static final String permissionDenie = "ไม่มีสิทธิ์สมัคร";
    public static final String policyNoOrRiderDoNotQualify =
        "สถานะกรมธรรม์ / rider ไม่เข้าเงื่อนไข";
    public static final String notMember = "ยังไม่ได้สมัคร";
  }

  public static class ErrorCode {
    /* errorCode CheckBDMS01 */
    public static final String BDMS01_0001 = "BDMS01-0001";
    public static final String BDMS01_0002 = "BDMS01-0002";
    public static final String BDMS01_0003 = "BDMS01-0003";
    public static final String BDMS01_0004 = "BDMS01-0004";
    public static final String BDMS01_0005 = "BDMS01-0005";
    public static final String BDMS01_0006 = "BDMS01-0006";
    /* errorCode RegisterBDMS02 */
    public static final String BDMS02_0001 = "BDMS02-0001";
    public static final String BDMS02_0002 = "BDMS02-0002";
    public static final String BDMS02_0003 = "BDMS02-0003";

    /* errorCode FamilyConsentBDMS03 */
    public static final String BDMS03_0001 = "BDMS03_0001";
    public static final String BDMS03_0002 = "BDMS03_0002";
    public static final String BDMS03_0003 = "BDMS03_0003";

  }
}
