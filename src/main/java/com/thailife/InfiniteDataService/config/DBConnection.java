package com.thailife.InfiniteDataService.config;

import lombok.extern.log4j.Log4j2;
import utility.database.SecuredConnect;

import java.sql.Connection;

@Log4j2
public class DBConnection {
  /* -------
  server: dev-odsdb.thailife.com
  port: 5432
  databaseName: customer
  username: customer.app
  password: Y2ZkNzk5M2MaYW
  -------------------------------------------------
  server: sit-odsdb.thailife.com
  port: 5432
  databaseName: customer
  username: customer.app
  password: P7HkNzk5M7MaYW
  --------------------------------------------
  server: uat-odsdb.thailife.com
  port: 5432
  databaseName: customer
  username: customer.app
  password: ADe8TwkYTuyB5G
  ------- */

  /* -------
  server: dev-odsdb.thailife.com
  port: 5433
  databaseName: policy
  username: policy.app
  password: ZTM4Yam5ZTJiYW
  -------------------------------------------------
  server: sit-odsdb.thailife.com
  port: 5433
  databaseName: policy
  username: policy.app
  password: HTM5Yam5HTJkYW
  --------------------------------------------
  server: uat-odsdb.thailife.com
  port: 5433
  databaseName: policy
  username: policy.app
  password: YzM3YWtyMzdkM2
  ------- */

  public static Connection getConnection(String DbName) throws Exception {
    Connection conn;
    try {
      conn = SecuredConnect.createConnection(DbName);
      /*throw new Exception("Test Error");*/
    } catch (Exception e) {
      log.error(String.format("Error Connect DB => %s", e.getMessage()));
      e.printStackTrace();
      throw e;
    }
    return conn;
  }
}
