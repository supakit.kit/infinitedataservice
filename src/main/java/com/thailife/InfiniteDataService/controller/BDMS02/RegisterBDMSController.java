package com.thailife.InfiniteDataService.controller.BDMS02;

import com.thailife.InfiniteDataService.model.commonModel.DataServiceRequestT;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceResponse;
import com.thailife.InfiniteDataService.model.requestM.RegisterBDMSRequestM;
import com.thailife.InfiniteDataService.service.RegisterBDMSService;
import com.thailife.InfiniteDataService.utility.Utils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.rmi.CORBA.Util;

@RestController
@Log4j2
public class RegisterBDMSController {
  @Autowired private RegisterBDMSService registerBDMSService;

  @PostMapping("/registerBDMS")
  public ResponseEntity<?> registerBDMSController(
      @RequestBody DataServiceRequestT<RegisterBDMSRequestM> request) {
    log.info(" [START] controller registerBDMS ==> ");
    DataServiceResponse response = new DataServiceResponse();
    try {
      response = registerBDMSService.service(request);
    } catch (Exception e) {
      log.error(" [controller]|errorMessage: " + e.getMessage());
    } finally {
      log.info(" [END] controller registerBDMS ==> ");
    }
    return ResponseEntity.status(Utils.getHttpCode(response.getResponseStatus())).body(response);
  }
}
