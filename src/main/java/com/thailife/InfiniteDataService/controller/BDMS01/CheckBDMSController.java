package com.thailife.InfiniteDataService.controller.BDMS01;

import com.thailife.InfiniteDataService.model.commonModel.DataServiceRequestT;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceResponseT;
import com.thailife.InfiniteDataService.model.requestM.CheckBDMSRequestM;
import com.thailife.InfiniteDataService.model.resonseM.CheckBDMSResponseM;
import com.thailife.InfiniteDataService.service.CheckBDMSService;
import com.thailife.InfiniteDataService.utility.Utils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
public class CheckBDMSController {
  @Autowired private CheckBDMSService checkBDMSService;

  @PostMapping("/checkBDMS")
  public ResponseEntity<?> checkBDMSController(
      @RequestBody DataServiceRequestT<CheckBDMSRequestM> request) {
    log.info(" [START] controller checkBDMS ==> ");
    DataServiceResponseT<CheckBDMSResponseM> response = new DataServiceResponseT<>();
    try {
      response = checkBDMSService.service(request);
    } catch (Exception e) {
      log.error(" [Controller]|errorMessage: " + e.getMessage());
    } finally {
      log.info(" [END] controller checkBDMS ==> ");
    }
    return ResponseEntity.status(Utils.getHttpCode(response.getResponseStatus())).body(response);
  }
}
