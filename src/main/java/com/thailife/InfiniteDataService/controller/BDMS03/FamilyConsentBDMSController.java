package com.thailife.InfiniteDataService.controller.BDMS03;

import com.thailife.InfiniteDataService.model.commonModel.DataServiceRequestT;
import com.thailife.InfiniteDataService.model.commonModel.DataServiceResponse;
import com.thailife.InfiniteDataService.model.requestM.FamilyConsentBDMSReqM;
import com.thailife.InfiniteDataService.service.FamilyConsentBDMSService;
import com.thailife.InfiniteDataService.utility.Utils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class FamilyConsentBDMSController {
  @Autowired private FamilyConsentBDMSService familyConsentBDMSService;

  @PostMapping("/familyConsentBDMS")
  public ResponseEntity<?> familyConsentBDMSController(
      @RequestBody DataServiceRequestT<FamilyConsentBDMSReqM> request) {
    log.info(" [START] controller familyConsentBDMS ==> ");
    DataServiceResponse response = new DataServiceResponse();
    try {
      response = familyConsentBDMSService.service(request);
    } catch (Exception e) {
      log.error(" [controller]|errorMessage: " + e.getMessage());
    } finally {
      log.info(" [END] controller familyConsentBDMS ==> ");
    }
    return ResponseEntity.status(Utils.getHttpCode(response.getResponseStatus())).body(response);
  }
}
