package com.thailife.InfiniteDataService.model.requestM;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class FamilyConsentBDMSReqM {
  private String familyId;
}
