package com.thailife.InfiniteDataService.model.requestM;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CheckBDMSRequestM {
  private String customerId;
}
