package com.thailife.InfiniteDataService.model.requestM;

import com.thailife.InfiniteDataService.model.AddressM;
import com.thailife.InfiniteDataService.model.FamilyM;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;

@Getter
@Setter
@ToString
public class RegisterBDMSRequestM {
  private String customerId;
  private String govtId;
  private String nationality;
  private String mobileNo;
  private String email;
  private Integer memberType;
  private String registerDate;
  private ArrayList<AddressM> address;
  private ArrayList<FamilyM> family;
}
