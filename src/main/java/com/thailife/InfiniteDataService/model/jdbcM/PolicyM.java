package com.thailife.InfiniteDataService.model.jdbcM;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class PolicyM {
  private String policyNo;
  private String certNo;
}
