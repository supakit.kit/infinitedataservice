package com.thailife.InfiniteDataService.model.jdbcM;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class MemberM {
  private String memberRegister;
  private String memberExp;
  private String memberCode;
  private Integer memberType;
  private String approve;
}
