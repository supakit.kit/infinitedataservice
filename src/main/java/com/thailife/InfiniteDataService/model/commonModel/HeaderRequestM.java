package com.thailife.InfiniteDataService.model.commonModel;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class HeaderRequestM {
	private HeaderData headerData;
}
