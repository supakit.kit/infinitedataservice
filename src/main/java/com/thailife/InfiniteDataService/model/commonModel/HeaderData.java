package com.thailife.InfiniteDataService.model.commonModel;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class HeaderData {
  private String messageId;
  private String sentDateTime;
  private String responseDateTime;

  public HeaderData(String messageId, String sentDateTime) {
    this.messageId = messageId;
    this.sentDateTime = sentDateTime;
  }
}
