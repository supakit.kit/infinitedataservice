package com.thailife.InfiniteDataService.model.commonModel;

import lombok.*;


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ResponseStatus {
	private String statusCode;
	private String errorCode;
	private String errorMessage;
}
