package com.thailife.InfiniteDataService.model.commonModel;


public class DataServiceRequestT<T>  extends HeaderRequestM {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public T requestRecord;

	public T getRequestRecord() {
		return requestRecord;
	}

	public void setRequestRecord(T requestRecord) {
		this.requestRecord = requestRecord;
	}

	
}
