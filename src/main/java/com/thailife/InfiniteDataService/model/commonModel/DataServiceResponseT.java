package com.thailife.InfiniteDataService.model.commonModel;


public class DataServiceResponseT<T>  extends HeaderResponseM {

	public T responseRecord;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataServiceResponseT() {
	}

	public DataServiceResponseT(T responseRecord) {
		this.responseRecord = responseRecord;
	}

	public T getResponseRecord() {
		return responseRecord;
	}

	public void setResponseRecord(T responseRecord) {
		this.responseRecord = responseRecord;
	}
}
