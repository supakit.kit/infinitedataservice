package com.thailife.InfiniteDataService.model;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class FamilyM extends AddressM {
  private String preName;
  private String firstName;
  private String lastName;
  private String birthDate;
  private String sex;
  private String mobileNo;
  private Integer relationCode;
  private String relation;
}
