package com.thailife.InfiniteDataService.model;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddressM {
  private String place;
  private String houseNo;
  private String building;
  private String floor;
  private String roomNo;
  private String village;
  private String villageNo;
  private String alley;
  private String road;
  private String district;
  private String subDistrict;
  private String zipCode;
  private String province;
}
