package com.thailife.InfiniteDataService.model.resonseM;

import com.thailife.InfiniteDataService.model.AddressM;
import lombok.*;

import java.util.ArrayList;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CheckBDMSResponseM {
  private String partyId;
  private String govtId;
  private String preName;
  private String firstNameTh;
  private String lastNameTh;
  private String firstNameEn;
  private String lastNameEn;
  private String dob;
  private String nationalityCode;
  private String mobileNo;
  private String email;
  private ArrayList<AddressM> address;
  private String memberRegister = "";
  private String memberExp = "";
  private String memberCode = "";
  private Integer statusRegister;
  /*
    1 : สมัครได้ infinite ,
    2 : สมัครได้ Health Fit DD ,
    3 : สมัครได้ VIP DD ,
    4 : สมัครได้ Health Fit Ultra
  */
  private Integer memberType;
  /*
    1 : ชีววัฒนะ Infinite ,
    2 : ชีววัฒนะ Health Fit DD ,
    3 : ชีววัฒนะ VIP DD ,
    4 : ชีววัฒนะ Health Fit Ultra

    ** ถ้ายังไม่ได้เป็นสมาชิก จะส่ง ""
  */

  public CheckBDMSResponseM(
      String partyId,
      String govtId,
      String preName,
      String firstNameTh,
      String lastNameTh,
      String firstNameEn,
      String lastNameEn,
      String dob,
      String nationalityCode) {
    this.partyId = partyId;
    this.govtId = govtId;
    this.preName = preName;
    this.firstNameTh = firstNameTh;
    this.lastNameTh = lastNameTh;
    this.firstNameEn = firstNameEn;
    this.lastNameEn = lastNameEn;
    this.dob = dob;
    this.nationalityCode = nationalityCode;
  }
}
